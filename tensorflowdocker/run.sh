#!/bin/sh

XSOCK=/tmp/.X11-unix
XAUTH=/home/$USER/.Xauthority
GIT_CONFIG=/home/$USER/.gitconfig
SSH_DIR=/home/$USER/.ssh


SSH_DIR=/home/$USER/.ssh

USER_ID=$(id -u)
WORKING_DIR_LAPTOP=/home/$USER/Documents/College/CSE842/Project/cse842project/
WORKING_DIR=/home/$USER/cse842project
HOME_DIR=/home/$USER
HOST_DIR=$(readlink -f /home/SUSER/)

echo $HOST_DIR

sudo docker run \
	-w $WORKING_DIR \
	--runtime=nvidia \
	-it --rm \
	--volume=$XSOCK:$XSOCK:rw \
	--volume=$XAUTH:$XAUTH:rw \
	--volume=$SSH_DIR:$SSH_DIR:Z \
	--volume=$GIT_CONFIG:$GIT_CONFIG:rw \
	--volume=$WORKING_DIR_LAPTOP:$WORKING_DIR:rw \
	--env="XAUTHORITY=${XAUTH}" \
	--env="DISPLAY=${DISPLAY}" \
	--env="TERM=xterm-256color" \
	-u $USER\
	--net=host \
	$USER/cse842-project-tensorflow

