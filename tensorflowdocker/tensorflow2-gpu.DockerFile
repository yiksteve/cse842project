
ARG UBUNTU_VERSION=18.04
ARG CUDA=10.0
FROM nvidia/cuda:${CUDA}-base-ubuntu${UBUNTU_VERSION} as base
# ARCH and CUDA are specified again because the FROM directive resets ARGs
# (but their default value is retained if set previously)
ARG CUDA
ARG CUDNN=7.6.2.24-1 
ARG USERNAME
ARG USERID

# Needed for string substitution 
SHELL ["/bin/bash", "-c"]
# Pick up some TF dependencies
RUN apt-get update && apt-get install -y \
        cuda-command-line-tools-${CUDA/./-} \
        cuda-cublas-${CUDA/./-} \
        cuda-cufft-${CUDA/./-} \
        cuda-curand-${CUDA/./-} \
        cuda-cusolver-${CUDA/./-} \
        cuda-cusparse-${CUDA/./-} \
        curl \
        libcudnn7=${CUDNN}+cuda${CUDA} \
	libfreetype6-dev \
        libhdf5-serial-dev \
        libzmq3-dev \ 
	pkg-config \
        software-properties-common \
        unzip \
	vim \
	tmux \
	eog \
	sudo apt-utils \
	ssh wget software-properties-common git cmake \
	xz-utils file locales dbus-x11 pulseaudio dmz-cursor-theme \
        fonts-dejavu fonts-liberation hicolor-icon-theme \
        libcanberra-gtk3-0 libcanberra-gtk-module libcanberra-gtk3-module \
        libasound2 libgtk2.0-0 libdbus-glib-1-2 libxt6 libexif12 \
        libgl1-mesa-glx libgl1-mesa-dri language-pack-en \
	gnome-terminal 


RUN [ ${ARCH} = ppc64le ] || (apt-get update && \
        apt-get install nvinfer-runtime-trt-repo-ubuntu1804-5.0.2-ga-cuda${CUDA} \
        && apt-get update \
        && apt-get install -y --no-install-recommends libnvinfer5=5.0.2-1+cuda${CUDA} \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/*)

# Add basic user
ENV USERNAME ${USERNAME}
ENV USERID ${USERID}

RUN useradd -m $USERNAME && \
        echo "$USERNAME:$USERNAME" | chpasswd && \
        usermod --shell /bin/bash $USERNAME && \
        usermod -aG dialout,sudo $USERNAME && \
        echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers && \
        chmod 0440 /etc/sudoers && \
        usermod  --uid $USERID $USERNAME

# Change user
USER $USERNAME

# Change default prompt
RUN sed 's|\(PS1=.*\[\\033.*\):\(.*\)|\1:\\\[\\033\[01;33m\\\]\[DOCKER\]\\\[\\033\[00m\\\]:\2|g' -i /home/$USERNAME/.bashrc
RUN sed '/033/b; s|PS1=\(.*\)\\u@\\h\(.*\)|PS1=\1\\u@\\h[DOCKER]\2|g' -i /home/$USERNAME/.bashrc
RUN echo 'set -g default-terminal "xterm-256color"' >> /home/$USERNAME/.tmux.conf

# For CUDA profiling, TensorFlow requires CUPTI.
ENV LD_LIBRARY_PATH /usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH

ARG USE_PYTHON_3_NOT_2=1
ARG _PY_SUFFIX=${USE_PYTHON_3_NOT_2:+3}
ARG PYTHON=python${_PY_SUFFIX}
ARG PIP=pip${_PY_SUFFIX}

# See http://bugs.python.org/issue19846
ENV LANG C.UTF-8
RUN echo "export QT_X11_NO_MITSHM=1" >> /home/$USERNAME/.bashrc

RUN sudo apt-get update && sudo apt-get install -y \
    ${PYTHON} \
    ${PYTHON}-pip


# Some TF tools expect a "python" binary
RUN sudo ln -s $(which ${PYTHON}) /usr/local/bin/python 

# Options:
#   tensorflow
#   tensorflow-gpu
#   tf-nightly
#   tf-nightly-gpu
# Set --build-arg TF_PACKAGE_VERSION=1.11.0rc0 to install a specific version.
# Installs the latest version by default.
ARG TF_PACKAGE=tensorflow-gpu
ARG TF_PACKAGE_VERSION=2.0.0rc0
RUN sudo curl -O https://bootstrap.pypa.io/get-pip.py
RUN sudo ${PYTHON} get-pip.py
RUN ${PYTHON} -m pip install --user --upgrade pip
RUN sudo ${PIP} --no-cache-dir install --upgrade \
    setuptools
RUN ${PIP} --version
ENV PATH="/home/$USERNAME/.local/bin:${PATH}"
RUN source ~/.bashrc
RUN ${PIP} install --user ${TF_PACKAGE}${TF_PACKAGE_VERSION:+==${TF_PACKAGE_VERSION}}
RUN ${PIP} install --user matplotlib
RUN ${PIP} install --user nltk
RUN ${PIP} install --user sklearn
CMD ["/bin/bash"]
