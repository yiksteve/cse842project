#!/bin/sh

# Build docker image
sudo docker build \
  --build-arg USERNAME=$USER \
  --build-arg USERID=$(id -u) \
  -t $USER/cse842-project-tensorflow -f tensorflow2-gpu.DockerFile .
