import utils.dataloader
import utils.model
import argparse
import math, pickle
import tensorflow as tf
import tensorflow.keras as keras

def pause():
    wait = input("PRESS ENTER TO CONTINUE.")

parser = argparse.ArgumentParser()
parser.add_argument('-l', '--load', action='store_true')
parser.add_argument('-p', '--path', type=str, default='', help="Path to tsv files for slang dataset")
opt = parser.parse_args()

if opt.load:
    with open('data.pkl', 'rb') as f:
        dataset_train, dataset_test = pickle.load(f)
    print('loaded dataset from files')
else:
    dataset_train = utils.dataloader.load_slang_ds(opt.path + "train.tsv")
    dataset_test = utils.dataloader.load_slang_ds(opt.path + "test.tsv")
    with open('data.pkl', 'wb') as f:
        pickle.dump([dataset_train, dataset_test], f)
    print('Made dataset dumps.')


slang_train = utils.dataloader.conv_text_as_int(dataset_train)
slang_test = utils.dataloader.conv_text_as_int(dataset_test)

print(slang_train.shape)
print(slang_test.shape)

pause()

seq_train = utils.dataloader.prep_sents(dataset_train)
seq_test = utils.dataloader.prep_sents(dataset_test)



pause()
model = utils.model.build_bilstm_model()
model.summary()

model.compile(optimizer='adam', loss=tf.keras.losses.SparseCategoricalCrossentropy(), metrics=['accuracy'])

model.fit_generator(generator = batch_gen(x_train, y_train, BATCH_SIZE, True),
        epochs = NUM_EPOCHS,
        validation_data = batch_gen(x_val, y_val, BATCH_SIZE, True),
        validation_steps = int(math.floor(x_val.shape[0]/BATCH_SIZE/(NUM_EPOCHS+1))),
        steps_per_epoch = int(math.floor(x_train.shape[0]/BATCH_SIZE/(NUM_EPOCHS+1))))

model.save('slang_lstm.h5')
