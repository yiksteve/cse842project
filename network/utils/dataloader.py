import sklearn
import tensorflow as tf
import csv
import numpy as np
import utils.model

def load_slang_ds(tsv_path):
    # [slang, explain, example, file_id]
    ds = []
    headers = ['slang','explanation', 'example', 'file_id']
    with open(tsv_path) as tsvfile:
        reader = csv.DictReader(tsvfile, fieldnames=headers, dialect='excel-tab')
        for row in reader:
           ds.append(row) 
    return ds

def conv_text_as_int(text):
    all_words = []
    all_vec = []
    for entry in text:
        all_words.append(entry['slang'])
    flat_list = [item for sublist in all_words for item in sublist]
    vocab = sorted(set(flat_list))
    char2idx = {u:i for i,u  in enumerate(vocab)} 
    idx2char = np.array(vocab)
    for slang in all_words:
        all_vec.append(list(char2idx[c] for c in slang))
    all_vec = tf.keras.preprocessing.sequence.pad_sequences(all_vec, maxlen=20)
    out = tf.convert_to_tensor(all_vec)
    return out

def prep_sents(text):
    all_sents = []
    for sents in text:
        all_sents.append(sents['example'])
    #TODO: Modifiy for generator cause mem overload
    all_sents = tf.keras.preprocessing.sequence.pad_sequences(all_sents, maxlen=4000)
    out = tf.convert_to_tensor(all_sents)
    return out

def batch_gen(slang, seq, definition, batch_size, shuffle):
    num_batches = slang.shape[0]
    counter = 0
    sample_idx = np.arange(x.shape[0])
    if shuffle:
        np.random.shuffle(sample_idx)
    while True:
        batch_idx = sample_idx[batch_size*counter:batch_size*(counter+1)]
        slang_batch = slang[batch_idx]
        seq = seq[batch_idx]
        x_batch = [slang_batch, seq_batch]
        y_batch = model.conv_text_as_glove(definition[batch_idx]) 
        counter += 1
        yield x_batch, y_batch
        if (counter == num_batches):
            if shuffle:
                np.random.shuffle(sample_idx)
            counter = 0



