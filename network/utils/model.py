import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.layers as layers
import os
import numpy as np

VOCAB_SIZE = 400000
NUM_CHARS = 20
EMBEDDING_DIM = 300
LSTM_DIM = 64
OUTPUT_SIZE = 50

def tokenize_words(dataset):
    explanations = []
    longest = 3482
    for dic in dataset:
        explanations.append(dic['example'])
    tokenizer.fit_on_texts(explanations)
    sequences = tokenizer.texts_to_sequences(explanations)
    # print(sequences[0])
    # sequences = keras.preprocessing.sequence.pad_sequences(sequences,maxlen=longest)
    return sequences


def build_bilstm_model():
    # TODO: change these to fit dataset

    word_input = keras.Input(shape=(None,), name='word')
    sent_input = keras.Input(shape=(None,), name='sent')

    word_vect = layers.Embedding(NUM_CHARS, EMBEDDING_DIM)(word_input)
    sent_vect = layers.Embedding(VOCAB_SIZE, EMBEDDING_DIM, embeddings_initializer=keras.initializers.Constant(embedding_weights), trainable=False)(sent_input)

    word_encoder = layers.LSTM(LSTM_DIM)(word_vect)
    sent_encoder = layers.LSTM(LSTM_DIM)(sent_vect)

    combine = layers.concatenate([word_encoder, sent_encoder])
    combine = tf.expand_dims(combine,axis=1) 
    explain_decoder = layers.LSTM(LSTM_DIM)(combine)

    explanation = layers.Dense(VOCAB_SIZE)(explain_decoder)

    model = keras.Model(inputs=[word_input, sent_input], outputs=[explanation])

    return model

def conv_text_as_glove(text):
    all_explainations = []
    conv_vec = []
    for entry in text:
        for word in entry['example']:
            conv_vec.append(word2vec[word])
        all_explainations.append(conv_vec)
        conv_vec = []
    out = tf.convert_to_tensor(all_explainations)
    return out

def load_pretrained_embedding(file_path, EMBEDDING_DIM, VOCAB_SIZE):
    with open(os.path.join(file_path+'.%sd.txt' % EMBEDDING_DIM), errors='ignore', encoding='utf8') as glove:
        for line in glove:
            values = line.split()
            word = values[0]
            vec = np.asarray(values[1:], dtype='float32')
            word2vec[word] = vec

        print('Found %s word vectors.' % len(word2vec))

        num_words = VOCAB_SIZE
        embedding_matrix = np.zeros((num_words, EMBEDDING_DIM))
        for word, i in tokenizer.word_index.items():
            if i > VOCAB_SIZE-1:
                break
            else:
                embedding_vector = word2vec.get(word)
                if embedding_vector is not None:
                    embedding_matrix[i] = embedding_vector
        return embedding_matrix


tokenizer = keras.preprocessing.text.Tokenizer(num_words = 400000)
word2vec = {}
embedding_weights = load_pretrained_embedding('./utils/glove/glove.6B', EMBEDDING_DIM, VOCAB_SIZE)
